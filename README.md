# TP2 BASH



## Question 1
```bash
tree rep_etudeSmall/ | wc -l
```
![tp2 bash](question 1 vrai .png)



## Question 2
```bash
tree 
```
![tp2 bash](tree question 2.png)

## Question 3
```bash
find -name *jpg
```
![tp2 bash](question 3.png)

## Question 4
```bash
find -name *jpg -o -name *png
```
![tp2 bash](question 4.png)

## Question 5
Je suis désolé je n'ai pas résussi à faire le script
```bash
ls -R rep_etude/ | grep -E *.jpg
```

## Question 6
Je suis désolé je n'ai pas résussi à faire le script

## Question 7
```bash
ls -R rep_etude/ | grep -Ei ".jpg"
```
## Question 8
```bash
mkdir OutPuts/ && for picture in `find rep_etude/ -type f -iname "*.jpg"`; do mv picture OutPuts/; done
```
## Question 9
```bash
for picture in `find OutPuts/ -type f -iname "*.jpg"`; do chmod 600 picture; done
```
## Question 10
```bash
du -sh rep_etude/
```
je n'ai pas réussi à faire la deuxième partie de la question

# TP3 BASH

## Question 1
```bash
for line in `find OutPuts/ -type f -name "*jpg"`; do echo "$line:`shasum $line | cut -d \" \" -f 1`" >> empreintes.txt; done
```
## Question 2
```bash
cat empreintes.txt | cut -d ":" -f 1
```
```bash
cat empreintes.txt|cut -d':' -f2-
```
## Question 3
```bash
for line in `cat empreintes.txt`; do echo `basename $line | cut -d ":" -f 1`; done
```
## Question 4
```bash
cpt=0
for fichier in $(find -type f -name "*.jpg")
do 
fichiernom=$(echo $fichier|rev|cut -d/ -f 1|rev|cut -c1-3)
cp $fichier /home/livreur/Documents/systeme/tp3/OutPutsQ4/'fichier_'$fichiernom'_num_'$cpt$($fichier|rev|cut -d/ -f 1|cut -c1-4|rev|tr '[:upper:]' '[:lower:]'
cpt=$((cpt+1))
done

```
## Question 5
```bash
dico=""; for line in `find OutPuts/ -type f`; do dico="$dico $line:`shasum $line | cut -d \" \" -f 1`"; done
```
## Question 6
```bash
pas réussi 
```
## Question 7
```bash
pas réussi
```
## Question 8
```bash
for fichier in $(find OutPutsQ8 -iname "*.png" -type f -o -iname "*.tif" -o -iname "*.gif" -o -iname "*.bmp")
do 
convert $fichier "${fichier%.*}.jpg";
done
rm OutPutsQ8/!(*.jpg)

```
## Question 9
```bash
pas réussi
```
## Question 10
```bash
pas réussi
```

